<?php
    $h= "<hr>";
    $x=15.53;
    $y=13;
    $x=$y=15.53;
    $x=$x+1;
    $x++;//Post Increment //
    $x+=1;

    echo $x++; //18.53
    echo $h;
    echo $x; //19.53
    echo $h;
    echo ++$x;  //20.53
    echo $h;
    echo $x; //20.53
    echo $h.$h;

    $m = 25;
    $n = 35;
    echo ++$m+$n++.$h; // 26+35=61 later $n becomes 36
    echo ++$m+$n--.$h; // 27+36=63 later $n becomes 35
    echo --$m+$n++.$h;// 26+35 =61 later $n becomes 36
    echo --$m+$n--.$h;// 25+36 =61 later $n becomes 35
    echo ++$m-$n++.$h;// 26-35=-9 later $n becomes 36
    echo ++$m-$n--.$h;// 27-36=-9 later $n becomes 35
    echo --$m-$n++.$h.$h;// 26-35 =-9 later $n becomes 36

    echo $m-- -$n--.$h;// 26-36 =-10 later $m becomes 25, $n becomes 35
    echo $m++ -$n--.$h;// 25-35 =-10 later $m becomes 26, $n becomes 34
    echo $m-- - --$n.$h;// 26-33 =--7 later $m becomes 25
    echo $m++ - ++$n.$h;// 25-34 =-9 later $m becomes 26
    echo $m++ - --$n.$h.$h;// 26-33 =-7 later $m becomes 27

    echo $m++ - --$m.$h; // 26-26= 0 Same variable expression right to left. From right --$m=26 then $m++=26 later becomes 27
    echo $m.$h.$h; // 27

    function divide($x,$y){
        return ++$x/$y; //28/33
    }

    echo  divide($m,$n);


?>