<?php
$input = "Alien";
echo str_pad($input,  6, "___");   // produces "Alien_"
echo str_pad($input, 10, "_", STR_PAD_BOTH);   // produces "__Alien___"
?>
