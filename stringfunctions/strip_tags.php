<?php
$text = '<p>Test paragraph.</p><!-- Comment --> ';
echo strip_tags($text);
echo "\n";

// Allow <p> and <a>
echo strip_tags($text, '<p><a>');
?>