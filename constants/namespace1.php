<?php
// application library 1
namespace App\PHP;

function MyFunction() {
    return __FUNCTION__;
}

class MyClass {
    static function WhoAmI() {
        return __METHOD__;
    }
}
echo "This file (".__FILE__.")"."Has a function ".MyFunction()."<br>"."<h1>"." Namespace is ".__NAMESPACE__."</h1>";
?>